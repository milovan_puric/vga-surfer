# VGA Surfer

<p> Implementation of a game surfer that will be displayed on a VGA display. The surfer can be in one of three lanes - middle, upper or lower. The surfer's position is changed by pressing the up and down buttons. The position of the surfer along the horizontal axis is fixed. Coins and bombs  are dropped from the other end of the screen at a randomly selected position and at random times. The surfer should pick up as many coins as possible (the number of collected coins is printed on LED displays) and avoid all bombs. If a bomb touches a surfer, the surfer loses one of the three lives he has until the end of the game. The number of remaining lives is displayed in the thermometer code on the LEDs. </p>

- HDL language: VHDL </br>
- Software: Intel Quartus Prime 18.1 </br>
- Board: DE1-SoC </br>
- Chip: Altera Cyclone V SE 5CSEMA5F31C6N </br>
- Peripheral: 1 × VGA, 2 × buttons, 3 × LE diods, 3 × LED display </br>

---

- Folder gameplay contains picture and video of started game. </br>
- Folder generate_mif_files contains bitmaps for items in game and their conversion to _.mif_ files. </br>
- Folder report contains report of project in serbian language. </br>
- Folder vga_surfer contains realisation of the project. </br>

