-------------------------------------------------------------------------------------------------------
--
-- Projekat: VGA surfer
-- Autori: Srdjan Djuricic 2016/655, Milovan Puric 2016/207
-- Fakultet  Elektrotehnicki fakultet Univerziteta u Beogradu
-- Katedra: Katedra za elektoniku
--
-------------------------------------------------------------------------------------------------------
--
-- Fajl: vga_surfer.vhd
-- Poslednja verzija napreavljena dana: 26.11.2019.
--
-------------------------------------------------------------------------------------------------------
--
-- Opis: Gnerator pseudo slucajnih brojeva u opsegu od 0 do 127.
--
-------------------------------------------------------------------------------------------------------


library ieee; 
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity rand_gen is 
port (
  clk           : in  std_logic;
  reset         : in  std_logic;
  enable        : in  std_logic;
  
  rand          : out integer range 0 to 127
 );
end rand_gen;

architecture behavioral of rand_gen is

constant poly_map : std_logic_vector(6 downto 0) := "1101001"; -- polinom: x^7 + x^6 + x^4 + x + 1

signal shift_reg : std_logic_vector(6 downto 0);

begin	
	rand <= to_integer(unsigned(shift_reg));
    
	generation: process (clk)
	begin
		if rising_edge(clk) then 
            if(reset='1') then
                shift_reg  <= poly_map;
            elsif (enable = '1') then 
              shift_reg(6) <= shift_reg(0);
              shift_reg(5) <= shift_reg(6) xor shift_reg(0);
              shift_reg(4) <= shift_reg(5);
              shift_reg(3) <= shift_reg(4) xor shift_reg(0);
              shift_reg(2) <= shift_reg(3);
              shift_reg(1) <= shift_reg(2);
              shift_reg(0) <= shift_reg(1) xor shift_reg(0);
            end if; 
		end if; 
	end process generation;

end architecture behavioral;