set_global_assignment -name FAMILY "Cyclone V"
set_global_assignment -name DEVICE 5CSEMA5F31C6
set_global_assignment -name DEVICE_FILTER_PACKAGE FBGA
set_global_assignment -name DEVICE_FILTER_PIN_COUNT 896
set_global_assignment -name DEVICE_FILTER_SPEED_GRADE 6
#============================================================
# CLOCK
#============================================================
set_location_assignment PIN_AF14 -to clk_50MHz
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to clk_50MHz
#============================================================
# SEG7
#============================================================
set_location_assignment PIN_AE26 -to displays_7seg[0]
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to displays_7seg[0]
set_location_assignment PIN_AE27 -to displays_7seg[1]
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to displays_7seg[1]
set_location_assignment PIN_AE28 -to displays_7seg[2]
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to displays_7seg[2]
set_location_assignment PIN_AG27 -to displays_7seg[3]
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to displays_7seg[3]
set_location_assignment PIN_AF28 -to displays_7seg[4]
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to displays_7seg[4]
set_location_assignment PIN_AG28 -to displays_7seg[5]
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to displays_7seg[5]
set_location_assignment PIN_AH28 -to displays_7seg[6]
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to displays_7seg[6]

set_location_assignment PIN_AJ29 -to displays_7seg[7]
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to displays_7seg[7]
set_location_assignment PIN_AH29 -to displays_7seg[8]
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to displays_7seg[8]
set_location_assignment PIN_AH30 -to displays_7seg[9]
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to displays_7seg[9]
set_location_assignment PIN_AG30 -to displays_7seg[10]
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to displays_7seg[10]
set_location_assignment PIN_AF29 -to displays_7seg[11]
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to displays_7seg[11]
set_location_assignment PIN_AF30 -to displays_7seg[12]
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to displays_7seg[12]
set_location_assignment PIN_AD27 -to displays_7seg[13]
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to displays_7seg[13]

set_location_assignment PIN_AB23 -to displays_7seg[14]
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to displays_7seg[14]
set_location_assignment PIN_AE29 -to displays_7seg[15]
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to displays_7seg[15]
set_location_assignment PIN_AD29 -to displays_7seg[16]
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to displays_7seg[16]
set_location_assignment PIN_AC28 -to displays_7seg[17]
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to displays_7seg[17]
set_location_assignment PIN_AD30 -to displays_7seg[18]
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to displays_7seg[18]
set_location_assignment PIN_AC29 -to displays_7seg[19]
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to displays_7seg[19]
set_location_assignment PIN_AC30 -to displays_7seg[20]
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to displays_7seg[20]
#============================================================
# KEY_N
#============================================================
set_location_assignment PIN_AA14 -to btn_down
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to btn_down
set_location_assignment PIN_AA15 -to btn_up
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to btn_up
#============================================================
# LED
#============================================================
set_location_assignment PIN_V16 -to life[0]
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to life[0]
set_location_assignment PIN_W16 -to life[1]
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to life[1]
set_location_assignment PIN_V17 -to life[2]
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to life[2]
#============================================================
# SW
#============================================================
set_location_assignment PIN_AB12 -to reset
set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to reset
#============================================================
# VGA
#============================================================
 set_location_assignment PIN_B13 -to VGA_B[0]
 set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to VGA_B[0]
 set_location_assignment PIN_G13 -to VGA_B[1]
 set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to VGA_B[1]
 set_location_assignment PIN_H13 -to VGA_B[2]
 set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to VGA_B[2]
 set_location_assignment PIN_F14 -to VGA_B[3]
 set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to VGA_B[3]
 set_location_assignment PIN_H14 -to VGA_B[4]
 set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to VGA_B[4]
 set_location_assignment PIN_F15 -to VGA_B[5]
 set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to VGA_B[5]
 set_location_assignment PIN_G15 -to VGA_B[6]
 set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to VGA_B[6]
 set_location_assignment PIN_J14 -to VGA_B[7]
 set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to VGA_B[7]
 set_location_assignment PIN_F10 -to VGA_BLANK_N
 set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to VGA_BLANK_N
 set_location_assignment PIN_A11 -to VGA_CLK
 set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to VGA_CLK
 set_location_assignment PIN_J9 -to VGA_G[0]
 set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to VGA_G[0]
 set_location_assignment PIN_J10 -to VGA_G[1]
 set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to VGA_G[1]
 set_location_assignment PIN_H12 -to VGA_G[2]
 set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to VGA_G[2]
 set_location_assignment PIN_G10 -to VGA_G[3]
 set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to VGA_G[3]
 set_location_assignment PIN_G11 -to VGA_G[4]
 set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to VGA_G[4]
 set_location_assignment PIN_G12 -to VGA_G[5]
 set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to VGA_G[5]
 set_location_assignment PIN_F11 -to VGA_G[6]
 set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to VGA_G[6]
 set_location_assignment PIN_E11 -to VGA_G[7]
 set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to VGA_G[7]
 set_location_assignment PIN_B11 -to VGA_HS
 set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to VGA_HS
 set_location_assignment PIN_A13 -to VGA_R[0]
 set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to VGA_R[0]
 set_location_assignment PIN_C13 -to VGA_R[1]
 set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to VGA_R[1]
 set_location_assignment PIN_E13 -to VGA_R[2]
 set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to VGA_R[2]
 set_location_assignment PIN_B12 -to VGA_R[3]
 set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to VGA_R[3]
 set_location_assignment PIN_C12 -to VGA_R[4]
 set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to VGA_R[4]
 set_location_assignment PIN_D12 -to VGA_R[5]
 set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to VGA_R[5]
 set_location_assignment PIN_E12 -to VGA_R[6]
 set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to VGA_R[6]
 set_location_assignment PIN_F13 -to VGA_R[7]
 set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to VGA_R[7]
 set_location_assignment PIN_C10 -to VGA_SYNC_N
 set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to VGA_SYNC_N
 set_location_assignment PIN_D11 -to VGA_VS
 set_instance_assignment -name IO_STANDARD "3.3-V LVTTL" -to VGA_VS