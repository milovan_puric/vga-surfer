-- Copyright (C) 2019  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and any partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details, at
-- https://fpgasoftware.intel.com/eula.

-- ***************************************************************************
-- This file contains a Vhdl test bench template that is freely editable to   
-- suit user's needs .Comments are provided in each section to help the user  
-- fill out necessary details.                                                
-- ***************************************************************************
-- Generated on "12/10/2019 12:13:18"
                                                            
-- Vhdl Test Bench template for design  :  rand_gen
-- 
-- Simulation tool : ModelSim-Altera (VHDL)
-- 

LIBRARY ieee;                                               
USE ieee.std_logic_1164.all;                                

ENTITY rand_gen_vhd_tst IS
END rand_gen_vhd_tst;
ARCHITECTURE rand_gen_arch OF rand_gen_vhd_tst IS
-- constants  
constant clk_period: time := 500 ms;                                                    
-- signals                                                   
SIGNAL clk : STD_LOGIC:= '0';
SIGNAL enable : STD_LOGIC;
SIGNAL rand : integer range 0 to 127;
SIGNAL reset : STD_LOGIC;
COMPONENT rand_gen
	PORT (
	clk : IN STD_LOGIC;
	enable : IN STD_LOGIC;
	rand : out integer range 0 to 127;
	reset : IN STD_LOGIC
	);
END COMPONENT;
BEGIN
	i1 : rand_gen
	PORT MAP (
-- list connections between master ports and signals
	clk => clk,
	enable => enable,
	rand => rand,
	reset => reset
	);

clk<= not clk after clk_period/2;                                        
                                      
always : PROCESS                                              
                                    
BEGIN  
	reset <= '1';
	enable <= '1';
	wait for 5*clk_period; 

	reset <= '0';
	enable <= '1';
	wait for 15*clk_period; 

	  
	                                                    

WAIT;                                                        
END PROCESS always;                                          
END rand_gen_arch;
