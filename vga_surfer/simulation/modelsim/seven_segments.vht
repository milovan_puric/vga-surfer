-- Copyright (C) 2019  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and any partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details, at
-- https://fpgasoftware.intel.com/eula.

-- ***************************************************************************
-- This file contains a Vhdl test bench template that is freely editable to   
-- suit user's needs .Comments are provided in each section to help the user  
-- fill out necessary details.                                                
-- ***************************************************************************
-- Generated on "12/02/2019 18:01:54"
                                                            
-- Vhdl Test Bench template for design  :  seven_segments
-- 
-- Simulation tool : ModelSim-Altera (VHDL)
-- 

LIBRARY ieee;                                               
USE ieee.std_logic_1164.all;                                

ENTITY seven_segments_vhd_tst IS
END seven_segments_vhd_tst;
ARCHITECTURE seven_segments_arch OF seven_segments_vhd_tst IS
-- constants
constant clk_period: time := 20 ms;                                                  
-- signals                                                   
SIGNAL clk : STD_LOGIC := '1';
SIGNAL displays_7seg : STD_LOGIC_VECTOR(20 DOWNTO 0);
SIGNAL number : INTEGER;
SIGNAL reset : STD_LOGIC;
COMPONENT seven_segments
	PORT (
	clk : IN STD_LOGIC;
	displays_7seg : BUFFER STD_LOGIC_VECTOR(20 DOWNTO 0);
	number : IN INTEGER;
	reset : IN STD_LOGIC
	);
END COMPONENT;
BEGIN
	i1 : seven_segments
	PORT MAP (
-- list connections between master ports and signals
	clk => clk,
	displays_7seg => displays_7seg,
	number => number,
	reset => reset
	);

clk <= not clk after clk_period/2;
                                     
always : PROCESS                                                                                  
BEGIN                                                         
        reset <= '1';
	number <= 89;
	wait for 20*clk_period;

	reset <= '0';
	number <= 0;
	wait for 20*clk_period;

	reset <= '0';
	number <= 5;
	wait for 20*clk_period;

	reset <= '0';
	number <= 89;
	wait for 20*clk_period;

	reset <= '0';
	number <= 123;
	wait for 20*clk_period;
	
	reset <= '0';
	number <= 999;
	wait for 20*clk_period;
	  
WAIT;                                                        
END PROCESS always;                                          
END seven_segments_arch;
