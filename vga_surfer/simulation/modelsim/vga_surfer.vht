-- Copyright (C) 2019  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and any partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details, at
-- https://fpgasoftware.intel.com/eula.

-- ***************************************************************************
-- This file contains a Vhdl test bench template that is freely editable to   
-- suit user's needs .Comments are provided in each section to help the user  
-- fill out necessary details.                                                
-- ***************************************************************************
-- Generated on "12/10/2019 11:44:02"
                                                            
-- Vhdl Test Bench template for design  :  vga_surfer
-- 
-- Simulation tool : ModelSim-Altera (VHDL)
-- 

LIBRARY ieee;                                               
USE ieee.std_logic_1164.all;                                

ENTITY vga_surfer_vhd_tst IS
END vga_surfer_vhd_tst;
ARCHITECTURE vga_surfer_arch OF vga_surfer_vhd_tst IS
-- constants  
constant clk_period: time := 20 ns;                                              
-- signals                                                   
SIGNAL btn_down : STD_LOGIC;
SIGNAL btn_up : STD_LOGIC;
SIGNAL clk_50MHz : STD_LOGIC:= '0';
SIGNAL displays_7seg : STD_LOGIC_VECTOR(20 DOWNTO 0);
SIGNAL life : STD_LOGIC_VECTOR(2 DOWNTO 0);
SIGNAL reset : STD_LOGIC;
SIGNAL VGA_B : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL VGA_BLANK_N : STD_LOGIC;
SIGNAL VGA_CLK : STD_LOGIC;
SIGNAL VGA_G : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL VGA_HS : STD_LOGIC;
SIGNAL VGA_R : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL VGA_SYNC_N : STD_LOGIC;
SIGNAL VGA_VS : STD_LOGIC;
COMPONENT vga_surfer
	PORT (
	btn_down : IN STD_LOGIC;
	btn_up : IN STD_LOGIC;
	clk_50MHz : IN STD_LOGIC;
	displays_7seg : BUFFER STD_LOGIC_VECTOR(20 DOWNTO 0);
	life : BUFFER STD_LOGIC_VECTOR(2 DOWNTO 0);
	reset : IN STD_LOGIC;
	VGA_B : BUFFER STD_LOGIC_VECTOR(7 DOWNTO 0);
	VGA_BLANK_N : BUFFER STD_LOGIC;
	VGA_CLK : BUFFER STD_LOGIC;
	VGA_G : BUFFER STD_LOGIC_VECTOR(7 DOWNTO 0);
	VGA_HS : BUFFER STD_LOGIC;
	VGA_R : BUFFER STD_LOGIC_VECTOR(7 DOWNTO 0);
	VGA_SYNC_N : BUFFER STD_LOGIC;
	VGA_VS : BUFFER STD_LOGIC
	);
END COMPONENT;
BEGIN
	i1 : vga_surfer
	PORT MAP (
-- list connections between master ports and signals
	btn_down => btn_down,
	btn_up => btn_up,
	clk_50MHz => clk_50MHz,
	displays_7seg => displays_7seg,
	life => life,
	reset => reset,
	VGA_B => VGA_B,
	VGA_BLANK_N => VGA_BLANK_N,
	VGA_CLK => VGA_CLK,
	VGA_G => VGA_G,
	VGA_HS => VGA_HS,
	VGA_R => VGA_R,
	VGA_SYNC_N => VGA_SYNC_N,
	VGA_VS => VGA_VS
	);

clk_50MHz <= not clk_50MHz after clk_period/2;                                        
always : PROCESS                                              
                                   
BEGIN                                                         
	reset <= '1';
	btn_down <= '0';
	btn_up <= '0';
	wait for 50*clk_period;

	reset <= '0';
	wait for 100000*clk_period;
WAIT;                                                        
END PROCESS always;                                          
END vga_surfer_arch;
