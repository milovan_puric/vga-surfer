-- Copyright (C) 2019  Intel Corporation. All rights reserved.
-- Your use of Intel Corporation's design tools, logic functions 
-- and other software and tools, and any partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Intel Program License 
-- Subscription Agreement, the Intel Quartus Prime License Agreement,
-- the Intel FPGA IP License Agreement, or other applicable license
-- agreement, including, without limitation, that your use is for
-- the sole purpose of programming logic devices manufactured by
-- Intel and sold by Intel or its authorized distributors.  Please
-- refer to the applicable agreement for further details, at
-- https://fpgasoftware.intel.com/eula.

-- ***************************************************************************
-- This file contains a Vhdl test bench template that is freely editable to   
-- suit user's needs .Comments are provided in each section to help the user  
-- fill out necessary details.                                                
-- ***************************************************************************
-- Generated on "12/02/2019 17:41:10"
                                                            
-- Vhdl Test Bench template for design  :  pozicija_surfera
-- 
-- Simulation tool : ModelSim-Altera (VHDL)
-- 

LIBRARY ieee;                                               
USE ieee.std_logic_1164.all;                                

ENTITY pozicija_surfera_vhd_tst IS
END pozicija_surfera_vhd_tst;
ARCHITECTURE pozicija_surfera_arch OF pozicija_surfera_vhd_tst IS
-- constants
constant clk_period: time := 1000 ms;                                                 
-- signals                                                   
SIGNAL btn_down : STD_LOGIC;
SIGNAL btn_up : STD_LOGIC;
SIGNAL clk : STD_LOGIC := '1';
SIGNAL reset : STD_LOGIC;
SIGNAL surfer_bot : STD_LOGIC;
SIGNAL surfer_mid : STD_LOGIC;
SIGNAL surfer_top : STD_LOGIC;
COMPONENT pozicija_surfera
	PORT (
	btn_down : IN STD_LOGIC;
	btn_up : IN STD_LOGIC;
	clk : IN STD_LOGIC;
	reset : IN STD_LOGIC;
	surfer_bot : OUT STD_LOGIC;
	surfer_mid : OUT STD_LOGIC;
	surfer_top : OUT STD_LOGIC
	);
END COMPONENT;
BEGIN
	i1 : pozicija_surfera
	PORT MAP (
-- list connections between master ports and signals
	btn_down => btn_down,
	btn_up => btn_up,
	clk => clk,
	reset => reset,
	surfer_bot => surfer_bot,
	surfer_mid => surfer_mid,
	surfer_top => surfer_top
	);

clk <= not clk after clk_period/2;
                                           
always : PROCESS                                                                                  
BEGIN                                                         
        reset <= '1';
	btn_down <= '0';
	btn_up <= '0';
	wait for 2000 ms; 

	reset <= '0';
	btn_down <= '0';
	btn_up <= '0';
	wait for 1000 ms; 

	reset <= '0';
	btn_down <= '1';
	btn_up <= '0';
	wait for 1000 ms; 

	reset <= '0';
	btn_down <= '0';
	btn_up <= '0';
	wait for 1000 ms;

	reset <= '0';
	btn_down <= '1';
	btn_up <= '0';
	wait for 1000 ms; 

	reset <= '0';
	btn_down <= '0';
	btn_up <= '0';
	wait for 1000 ms;

	reset <= '0';
	btn_down <= '0';
	btn_up <= '1';
	wait for 1000 ms;

	reset <= '0';
	btn_down <= '0';
	btn_up <= '0';
	wait for 1000 ms;

	reset <= '0';
	btn_down <= '0';
	btn_up <= '1';
	wait for 1000 ms;

	reset <= '0';
	btn_down <= '0';
	btn_up <= '0';
	wait for 1000 ms;

	reset <= '0';
	btn_down <= '0';
	btn_up <= '1';
	wait for 1000 ms;

	reset <= '0';
	btn_down <= '0';
	btn_up <= '0';
	wait for 1000 ms;

	reset <= '0';
	btn_down <= '1';
	btn_up <= '0';
	wait for 1000 ms; 

	reset <= '0';
	btn_down <= '0';
	btn_up <= '0';
	wait for 1000 ms;
WAIT;                                                        
END PROCESS always;                                          
END pozicija_surfera_arch;
