---------------------------------------------------------------------------------------------------
--
-- Projekat: VGA surfer
-- Autori: Srdjan Djuricic 2016/655, Milovan Puric 2016/207
-- Fakultet  Elektrotehnicki fakultet Univerziteta u Beogradu
-- Katedra: Katedra za elektoniku
--
---------------------------------------------------------------------------------------------------
--
-- Fajl: crtanje_i_sudari.vhd
-- Poslednja verzija napreavljena dana: 8.12.2019.
--
---------------------------------------------------------------------------------------------------
--
-- Opis: Opsiuje kretanje i interakciju surfera i sa druge strane bombi i novicica
--
---------------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity crtanje_i_sudari is
port (
	clk                          :  in std_logic;
	reset                        :  in std_logic;
    
	hpos                         :  in integer range 0 to 1023;
	vpos                         :  in integer range 0 to 767;
   ref_tick                     :  in std_logic;
	Rout, Gout, Bout             : out std_logic_vector(7 downto 0);
    
	surf_top, surf_mid, surf_bot :  in std_logic;
	random_num                   :  in integer range 0 to 127;
   score_out                    : out integer range 0 to 999;
   life_out                     : out std_logic_vector(2 downto 0)
);
end crtanje_i_sudari;

architecture behavioral of crtanje_i_sudari is

-- jednoportna ROM memorija u kojoj se nalazi slika surfera
component bitmap_surfer
	PORT
	(
		address		: IN STD_LOGIC_VECTOR (11 DOWNTO 0);
		clock		: IN STD_LOGIC  := '1';
		q		    : OUT STD_LOGIC_VECTOR (11 DOWNTO 0)
	);
end component;

-- jednoportna ROM memorija u kojoj se nalazi slika bombe
component bitmap_bomba
	PORT
	(
		address		: IN STD_LOGIC_VECTOR (11 DOWNTO 0);
		clock		: IN STD_LOGIC  := '1';
		q		    : OUT STD_LOGIC_VECTOR (11 DOWNTO 0)
	);
end component;

-- jednoportna ROM memorija u kojoj se nalazi slika novcica
component bitmap_novcic
	PORT
	(
		address		: IN STD_LOGIC_VECTOR (11 DOWNTO 0);
		clock		: IN STD_LOGIC  := '1';
		q		    : OUT STD_LOGIC_VECTOR (11 DOWNTO 0)
	);
end component;

	constant OBJECT_DIM      : integer :=   48; -- velicina svih objekata: surfera, novcica i bombi
    
   constant SURFER_H_START  : integer :=   16; -- vertikalna linija od kojoj pocinje iscrtavanje surfera
	constant COLLECT_H_START : integer := 1024; -- vertikalna linija od kojoj pocinje iscrtavanje novcica i bombi
    
   constant OBJECT_V_START_TOP : integer := 104; -- horizontalna linija od koje pocinje iscrtavanje objekata koji se nalaze na gornjoj traci
   constant OBJECT_V_START_MID : integer := 360; -- horizontalna linija od koje pocinje iscrtavanje objekata koji se nalaze na srednjoj traci
   constant OBJECT_V_START_BOT : integer := 616; -- horizontalna linija od koje pocinje iscrtavanje objekata koji se nalaze na donjoj traci
	  
	constant BCKG_COLOR   : std_logic_vector(23 downto 0) := x"3399FF";
--	  constant SURFER_COLOR : std_logic_vector(23 downto 0) := x"FFA500";
--   constant COIN_COLOR   : std_logic_vector(23 downto 0) := x"FFFF00";
--   constant BOMB_COLOR   : std_logic_vector(23 downto 0) := x"000000";
	
	signal color          : std_logic_vector(23 downto 0);
    
   -- kako se u jednom trenutku na ekranu moze naci najvise 8 "collectables-a" (novcica i bombi) kreira se 3 niza sa po 8 elemenata, 
   -- od kojih svaki predstavlja jedan collectables koji treba da se iscrtava: jedan u koji se smesta tip collectablesa i dva u koje 
   -- se smestaju x i y kooridnate svakog elementa posebno.
   type collect_type is (bomba, novcic, nista);                
   type array_collect_type is array (0 to 7) of collect_type;
   signal collect_tip: array_collect_type;
    
   type array_collect_xpos is array (0 to 7) of integer range -OBJECT_DIM to 1023 + OBJECT_DIM;
   type array_collect_ypos is array (0 to 7) of integer range 104 to 616;
   signal collect_xpos: array_collect_xpos;
   signal collect_ypos: array_collect_ypos;
   signal surf_ypos : integer range 104 to 616;
    
   -- promenljive koje ce sluziti kao izlaz dekodera nasumicnih brojeva
   type collect_type_loc is (bomba_top, bomba_mid, bomba_bot, novcic_top, novcic_mid, novcic_bot);
   signal decoded_collect: collect_type_loc;
    
   -- brzina collectablesa
   signal collect_xspeed : integer range -8 to 0;
	
   -- podatak da li se trenutno nalazimo na surferu ili na collectables
	signal on_surfer : std_logic;
   signal on_collect: std_logic_vector(0 to 7);
    
   signal score : integer range 0 to 999;
   signal life  : std_logic_vector(2 downto 0);
    
   -- izlazi ROM memorija:
   signal datamem_surfer : std_logic_vector(11 downto 0);
   signal datamem_bomba  : std_logic_vector(11 downto 0);
   signal datamem_novcic : std_logic_vector(11 downto 0);
    
   type address_array is array (0 to 7) of unsigned(11 downto 0); --/ adrese za bitmape collectables
   signal addresses : address_array;                              --/ 
   signal address_bomb   : unsigned(11 downto 0);                 --/
   signal address_novcic : unsigned(11 downto 0);                 --/
   signal address : unsigned(11 downto 0); -- adresa za bitmapu surfera
    
begin

-- U zavisnosti od pozicije surfera treba nacrtati na odredjenom delu ekrana. Promenljiva "on_surfer" ce nam kasnije sluziti za crtanje bitmape na mestu na kom se trenutno nalazi surfer.                
crtanje_surfera: process (clk, surf_top, surf_mid, surf_bot, hpos, vpos) is
begin
    if (rising_edge(clk)) then
        if (surf_top = '1') then
            surf_ypos <= OBJECT_V_START_TOP;
            if ((hpos > SURFER_H_START and hpos <= SURFER_H_START + OBJECT_DIM) and (vpos > OBJECT_V_START_TOP and vpos <= OBJECT_V_START_TOP + OBJECT_DIM)) then
                on_surfer <= '1'; 
            else
                on_surfer <= '0';
            end if;
        elsif (surf_mid = '1') then
            surf_ypos <= OBJECT_V_START_MID;
            if ((hpos > SURFER_H_START and hpos <= SURFER_H_START + OBJECT_DIM) and (vpos > OBJECT_V_START_MID and vpos <= OBJECT_V_START_MID + OBJECT_DIM)) then
                on_surfer <= '1';
            else
                on_surfer <= '0';
            end if;
        elsif (surf_bot = '1') then
            surf_ypos <= OBJECT_V_START_BOT;
            if ((hpos > SURFER_H_START and hpos <= SURFER_H_START + OBJECT_DIM) and (vpos > OBJECT_V_START_BOT and vpos <= OBJECT_V_START_BOT + OBJECT_DIM)) then
                on_surfer <= '1';
            else
                on_surfer <= '0';
            end if;
        end if;
    end if;
end process crtanje_surfera;

-- U zavisnosti od pozicije collectables-a treba ih nacrtati na odredjenom delu ekrana. Promenljiva "on_collect(i)" ce nam kasnije sluziti za crtanje bitmape na mestu na kom se trenutno nalazi collectable. 
crtanje_collectables: process (clk, hpos, vpos, collect_xpos, collect_ypos) is
begin
    if (rising_edge(clk)) then
        for i in 0 to 7 loop
            if ((hpos > collect_xpos(i) and hpos <= collect_xpos(i) + OBJECT_DIM) and (vpos > collect_ypos(i) and vpos <= collect_ypos(i) + OBJECT_DIM) and (collect_tip(i) /= nista)) then
                on_collect(i) <= '1';
            else
                on_collect(i) <= '0';
            end if;
        end loop;
    end if;
end process crtanje_collectables;

-- Na osnovu izlaza genratora pseudo-slucajnih brojeva, treba odrediti gde i sta ce se od coolectablesa (novcic ili bomba) pojaviti na keranu.
dekodovanje_pozicije_collectables: process(clk, random_num) is
begin
    if (rising_edge(clk)) then
        if    (random_num <= 18) then
            decoded_collect <= bomba_top;
        elsif (random_num <= 36) then
            decoded_collect <= bomba_mid;
        elsif (random_num <= 55) then
            decoded_collect <= bomba_bot;
        elsif (random_num <= 79) then
            decoded_collect <= novcic_top;
        elsif (random_num <= 103) then
            decoded_collect <= novcic_mid;
        else
            decoded_collect <= novcic_bot;
        end if;
    end if;
end process dekodovanje_pozicije_collectables;

-- Logika za generisanje, unistavanje, kretanje collectables-a, kao i sudare collectables-a sa surferom.
collectables_logic: process(clk, collect_tip, collect_xpos) is
begin
    if (rising_edge(clk)) then
        if (reset = '1') then
            collect_tip <= (others => nista);
            collect_xpos <= (others => COLLECT_H_START);
            score <= 0;
            life <= "111";
        elsif (life /= "000") then
            -- GENERISANJE NOVCICA I BOMBI:
            
            -- Ako su svi coolectables prazni znaci da smo na pocetku igre i bezuslovno treba generisati prvi collectables:
            if ((collect_tip(0) = nista) and (collect_tip(1) = nista) and (collect_tip(2) = nista) and (collect_tip(3) = nista) and 
                (collect_tip(4) = nista) and (collect_tip(5) = nista) and (collect_tip(6) = nista) and (collect_tip(7) = nista)) then
                collect_xpos(0) <= COLLECT_H_START; -- mislim da ti ovo ne treba
                case decoded_collect is
                    when bomba_top => collect_ypos(0) <= OBJECT_V_START_TOP;
                                      collect_tip(0) <= bomba;
                    when bomba_mid => collect_ypos(0) <= OBJECT_V_START_MID;
                                      collect_tip(0) <= bomba;
                    when bomba_bot => collect_ypos(0) <= OBJECT_V_START_BOT;
                                      collect_tip(0) <= bomba;
                    when novcic_top => collect_ypos(0) <= OBJECT_V_START_TOP;
                                       collect_tip(0) <= novcic;
                    when novcic_mid => collect_ypos(0) <= OBJECT_V_START_MID;
                                       collect_tip(0) <= novcic;
                    when novcic_bot => collect_ypos(0) <= OBJECT_V_START_BOT;
                                       collect_tip(0) <= novcic;
                end case;
            -- Ako na ekranu vec postoji makara jedan collectable, to znaci da sledeci treba generisati tek ako je ovaj pre njega presao odredjenu granicu (880px) 
            -- i ako taj koji treba da se generise nije vec generisan, tj. ako je tipa "nista"
            else 
                for i in 0 to 7 loop
                    if (collect_xpos(i) <= (COLLECT_H_START-3*OBJECT_DIM)) then 
                        -- Napisan je posebni slucaj ako smo stigli do poslednjeg bafera u koji smestamo collectable, to znaci da je sledeci na redu za popunjavanje prvi bafer,
                        -- u suprotnom se samo uzima bafer posle poseldnjeg generisanog.
                        if (i = 7) then
                            if (collect_tip(0) = nista) then 
                                collect_xpos(0) <= COLLECT_H_START; -- mislim da ti ovo ne treba
                                case decoded_collect is
                                    when bomba_top => collect_ypos(0) <= OBJECT_V_START_TOP;
                                                      collect_tip(0) <= bomba;
                                    when bomba_mid => collect_ypos(0) <= OBJECT_V_START_MID;
                                                      collect_tip(0) <= bomba;
                                    when bomba_bot => collect_ypos(0) <= OBJECT_V_START_BOT;
                                                      collect_tip(0) <= bomba;
                                    when novcic_top => collect_ypos(0) <= OBJECT_V_START_TOP;
                                                       collect_tip(0) <= novcic;
                                    when novcic_mid => collect_ypos(0) <= OBJECT_V_START_MID;
                                                       collect_tip(0) <= novcic;
                                    when novcic_bot => collect_ypos(0) <= OBJECT_V_START_BOT;
                                                       collect_tip(0) <= novcic;
                                end case;
                            end if;
                        else
                            if (collect_tip(i+1) = nista) then 
                                collect_xpos(i+1) <= COLLECT_H_START; -- mislim da ti ovo ne treba
                                case decoded_collect is
                                    when bomba_top => collect_ypos(i+1) <= OBJECT_V_START_TOP;
                                                      collect_tip(i+1) <= bomba;
                                    when bomba_mid => collect_ypos(i+1) <= OBJECT_V_START_MID;
                                                      collect_tip(i+1) <= bomba;
                                    when bomba_bot => collect_ypos(i+1) <= OBJECT_V_START_BOT;
                                                      collect_tip(i+1) <= bomba;
                                    when novcic_top => collect_ypos(i+1) <= OBJECT_V_START_TOP;
                                                       collect_tip(i+1) <= novcic;
                                    when novcic_mid => collect_ypos(i+1) <= OBJECT_V_START_MID;
                                                       collect_tip(i+1) <= novcic;
                                    when novcic_bot => collect_ypos(i+1) <= OBJECT_V_START_BOT;
                                                       collect_tip(i+1) <= novcic;
                                end case;
                            end if;
                        end if;
                    end if;
                end loop;
            end if;
            
            -- UNISTAVANJE NOVCICA I BOMBI:
            
            -- Kada collecable stigne do kraja treba ga restartovati, tj. spremiti za kandidata za ponovno generisanje.
            for i in 0 to 7 loop
                if ((collect_xpos(i) >= -OBJECT_DIM) and (collect_xpos(i) <= (SURFER_H_START - OBJECT_DIM))) then 
                    collect_tip(i) <= nista;
                    collect_xpos(i) <= COLLECT_H_START; 
                end if;
            end loop;

            -- KRETANJE NOVCICA I BOMBI:
            
            -- Na svaki novi refresh rate ekrana treba povecati trenutni polozaj collectables-a za trenutnu brzinu pod uslovom da on nije tipa "nista".
            if (ref_tick = '1') then
                for i in 0 to 7 loop
                    if (collect_tip(i) /= nista) then
                        collect_xpos(i) <= collect_xpos(i) + collect_xspeed;
                    end if;
                end loop;
            end if;
            
            -- SUDARI NOVCICA I BOMBI SA SURFEROM:
            
            -- Ako se collectables nalazi u isto vreme na istom mestu kao i surfer, u zavisnosti od toga kog je tipa taj collecable cemo povecati skor ako je to bio novcic ili oduzeti jedan zivot ako je to bila bomba.
            -- Takodje, tu su i granicni slucajevi: kada je skor presao 999 i kada se izgubi poseldnji zivot, kada se igrica resetuje.
            for i in 0 to 7 loop
                if ((collect_xpos(i) <= (SURFER_H_START + OBJECT_DIM)) and (collect_xpos(i) >= (SURFER_H_START - OBJECT_DIM + 3)) and (collect_ypos(i) = surf_ypos)) then 
                    case collect_tip(i) is
                        when novcic =>
                            if (score = 999) then
                                score <= 0;
                                collect_tip <= (others => nista);
                                collect_xpos <= (others => COLLECT_H_START);
                            else 
                                score <= score + 1;
                                collect_tip(i) <= nista;
                                collect_xpos(i) <= COLLECT_H_START;
                            end if;
                        when bomba =>
                                case life is
                                    when "001" =>                -- imas 1 zivot ides na 0 i resetuje se igrica
													 score <= 0;
                                        life <= "000";
                                        collect_tip <= (others => nista);
                                        collect_xpos <= (others => COLLECT_H_START); 
                                    when "011" => 
                                        life <= "001";           -- imas 2 zivota ides na 1
                                        collect_tip(i) <= nista;
                                        collect_xpos(i) <= COLLECT_H_START;
                                    when "111" => 
                                        life <= "011";           -- imas 3 zivota ides na 2
                                        collect_tip(i) <= nista;
                                        collect_xpos(i) <= COLLECT_H_START;
                                    when others =>
                                        null;
                                end case;
                        when nista =>
                            null;
                    end case;
                end if;
            end loop;
            
        end if;   
    end if;
end process collectables_logic;

-- Brzina se menja u zavisnosi od trenutnog skora:
azuriranje_brzine: process (clk, score) is
begin
    if (rising_edge(clk)) then
        if (reset = '1') then
            collect_xspeed <= 0;
        else
            if    (score <= 15) then
                collect_xspeed <= -3;
            elsif (score <= 30) then
                collect_xspeed <= -4;
            elsif (score <= 45) then
                collect_xspeed <= -5;
            elsif (score <= 60) then
                collect_xspeed <= -6;
            elsif (score <= 75) then
                collect_xspeed <= -7;
            else
                collect_xspeed <= -8;
            end if;
        end if;
    end if;
end process azuriranje_brzine;

-- Racunanje adresa u zavisnoti od trenutnog polozaja objekata na ekranu:
address_calculation: process(clk)
begin
    if (rising_edge(clk)) then
    
        if (reset = '1') then
            address <= (others => '0');
            addresses <= (others => "000000000000"); 
        else
            if (on_surfer = '1') then
                if ((hpos = SURFER_H_START + OBJECT_DIM - 1) and (vpos = surf_ypos + OBJECT_DIM - 1)) then
                    address <= (others => '0');
                else
                    address <= address + 1;
                end if;
            end if;
                
        for i in 0 to 7 loop
            if (on_collect(i) = '1') then
                if ((hpos = collect_xpos(i) + OBJECT_DIM - 1) and (vpos = collect_ypos(i) + OBJECT_DIM - 1)) then
                    addresses(i) <= (others => '0');
                else
                    addresses(i) <= addresses(i) + 1;
                end if;
            end if;
        end loop;
                
    end if;
end if;
end process;

-- Postavljanje odgovarajucih bitmapa (slika) na ekran u zavisnosti kog su tipa trenutni objekti:
bojenje: process (clk, on_surfer, on_collect) is
begin
    if (rising_edge(clk)) then
        if (reset = '1') then
            color <= (others => '0'); 
        else
            if (on_surfer = '1') then
                -- Ako je procitana crna boja, umesto nje treba staviti boju pozadine, kako ne bismo imali okvire za nase slike objekata. 
                -- U suprotnom na ekran staviti odgovarajuci piksel slike.
                if datamem_surfer /= x"000" then
                    color <= datamem_surfer(11 downto 8) & x"0" & datamem_surfer(7 downto 4) & x"0" & datamem_surfer(3 downto 0) & x"0"; -- dodaju se 4 nule na kraj svih komponenti boja zato sto boje prikazujemo na 4, a ne na 8 bita.
                else
                    color <= BCKG_COLOR;
                end if;
            elsif (on_collect(0) = '1') then
                case collect_tip(0) is
                        when bomba  => 
                            address_bomb <= addresses(0);
                            if datamem_bomba /= x"000" then
                                color <= datamem_bomba(11 downto 8) & x"0" & datamem_bomba(7 downto 4) & x"0" & datamem_bomba(3 downto 0) & x"0";
                            else
                                color <= BCKG_COLOR;
                            end if;
                        when novcic =>
                            address_novcic <= addresses(0); 
                            if datamem_novcic /= x"000" then
                                color <= datamem_novcic(11 downto 8) & x"0" & datamem_novcic(7 downto 4) & x"0" & datamem_novcic(3 downto 0) & x"0";
                            else
                                color <= BCKG_COLOR;
                            end if;
                       when nista  => 
                            color <= BCKG_COLOR;
                    end case;
				elsif (on_collect(1) = '1') then
                case collect_tip(1) is
                        when bomba  => 
                            address_bomb <= addresses(1);
                            if datamem_bomba /= x"000" then
                                color <= datamem_bomba(11 downto 8) & x"0" & datamem_bomba(7 downto 4) & x"0" & datamem_bomba(3 downto 0) & x"0";
                            else
                                color <= BCKG_COLOR;
                            end if;
                        when novcic =>
                            address_novcic <= addresses(1); 
                            if datamem_novcic /= x"000" then
                                color <= datamem_novcic(11 downto 8) & x"0" & datamem_novcic(7 downto 4) & x"0" & datamem_novcic(3 downto 0) & x"0";
                            else
                                color <= BCKG_COLOR;
                            end if;
                       when nista  => 
                            color <= BCKG_COLOR;
                    end case;
				elsif (on_collect(2) = '1') then
                case collect_tip(2) is
                        when bomba  => 
                            address_bomb <= addresses(2);
                            if datamem_bomba /= x"000" then
                                color <= datamem_bomba(11 downto 8) & x"0" & datamem_bomba(7 downto 4) & x"0" & datamem_bomba(3 downto 0) & x"0";
                            else
                                color <= BCKG_COLOR;
                            end if;
                        when novcic =>
                            address_novcic <= addresses(2); 
                            if datamem_novcic /= x"000" then
                                color <= datamem_novcic(11 downto 8) & x"0" & datamem_novcic(7 downto 4) & x"0" & datamem_novcic(3 downto 0) & x"0";
                            else
                                color <= BCKG_COLOR;
                            end if;
                       when nista  => 
                            color <= BCKG_COLOR;
                    end case;
				elsif (on_collect(3) = '1') then
                case collect_tip(3) is
                        when bomba  => 
                            address_bomb <= addresses(3);
                            if datamem_bomba /= x"000" then
                                color <= datamem_bomba(11 downto 8) & x"0" & datamem_bomba(7 downto 4) & x"0" & datamem_bomba(3 downto 0) & x"0";
                            else
                                color <= BCKG_COLOR;
                            end if;
                        when novcic =>
                            address_novcic <= addresses(3); 
                            if datamem_novcic /= x"000" then
                                color <= datamem_novcic(11 downto 8) & x"0" & datamem_novcic(7 downto 4) & x"0" & datamem_novcic(3 downto 0) & x"0";
                            else
                                color <= BCKG_COLOR;
                            end if;
                       when nista  => 
                            color <= BCKG_COLOR;
                    end case;
				elsif (on_collect(4) = '1') then
                case collect_tip(4) is
                        when bomba  => 
                            address_bomb <= addresses(4);
                            if datamem_bomba /= x"000" then
                                color <= datamem_bomba(11 downto 8) & x"0" & datamem_bomba(7 downto 4) & x"0" & datamem_bomba(3 downto 0) & x"0";
                            else
                                color <= BCKG_COLOR;
                            end if;
                        when novcic =>
                            address_novcic <= addresses(4); 
                            if datamem_novcic /= x"000" then
                                color <= datamem_novcic(11 downto 8) & x"0" & datamem_novcic(7 downto 4) & x"0" & datamem_novcic(3 downto 0) & x"0";
                            else
                                color <= BCKG_COLOR;
                            end if;
                       when nista  => 
                            color <= BCKG_COLOR;
                    end case;
				elsif (on_collect(5) = '1') then
                case collect_tip(5) is
                        when bomba  => 
                            address_bomb <= addresses(5);
                            if datamem_bomba /= x"000" then
                                color <= datamem_bomba(11 downto 8) & x"0" & datamem_bomba(7 downto 4) & x"0" & datamem_bomba(3 downto 0) & x"0";
                            else
                                color <= BCKG_COLOR;
                            end if;
                        when novcic =>
                            address_novcic <= addresses(5); 
                            if datamem_novcic /= x"000" then
                                color <= datamem_novcic(11 downto 8) & x"0" & datamem_novcic(7 downto 4) & x"0" & datamem_novcic(3 downto 0) & x"0";
                            else
                                color <= BCKG_COLOR;
                            end if;
                       when nista  => 
                            color <= BCKG_COLOR;
                    end case;
				elsif (on_collect(6) = '1') then
                case collect_tip(6) is
                        when bomba  => 
                            address_bomb <= addresses(6);
                            if datamem_bomba /= x"000" then
                                color <= datamem_bomba(11 downto 8) & x"0" & datamem_bomba(7 downto 4) & x"0" & datamem_bomba(3 downto 0) & x"0";
                            else
                                color <= BCKG_COLOR;
                            end if;
                        when novcic =>
                            address_novcic <= addresses(6); 
                            if datamem_novcic /= x"000" then
                                color <= datamem_novcic(11 downto 8) & x"0" & datamem_novcic(7 downto 4) & x"0" & datamem_novcic(3 downto 0) & x"0";
                            else
                                color <= BCKG_COLOR;
                            end if;
                       when nista  => 
                            color <= BCKG_COLOR;
                    end case;
				elsif (on_collect(7) = '1') then
                case collect_tip(7) is
                        when bomba  => 
                            address_bomb <= addresses(7);
                            if datamem_bomba /= x"000" then
                                color <= datamem_bomba(11 downto 8) & x"0" & datamem_bomba(7 downto 4) & x"0" & datamem_bomba(3 downto 0) & x"0";
                            else
                                color <= BCKG_COLOR;
                            end if;
                        when novcic =>
                            address_novcic <= addresses(7); 
                            if datamem_novcic /= x"000" then
                                color <= datamem_novcic(11 downto 8) & x"0" & datamem_novcic(7 downto 4) & x"0" & datamem_novcic(3 downto 0) & x"0";
                            else
                                color <= BCKG_COLOR;
                            end if;
                       when nista  => 
                            color <= BCKG_COLOR;
                    end case;
            else 
					color <= BCKG_COLOR;
            end if;
      
		end if;
    end if;
end process bojenje;

--Boji jednobojno:
--bojenje: process (clk, on_surfer, on_collect) is
--begin
--    if (rising_edge(clk)) then
--        if (reset = '1') then
--            color <= (others => '0'); 
--        else
--            if (on_surfer = '1') then
--                color <= SURFER_COLOR;
--            else 
--                if (on_collect(0) = '1') then
--						case collect_tip(0) is
--							when bomba  => color <= BOMB_COLOR;
--							when novcic => color <= COIN_COLOR;
--							when nista  => color <= BCKG_COLOR;
--                  end case;
--					 elsif (on_collect(1) = '1') then
--						case collect_tip(1) is
--							when bomba  => color <= BOMB_COLOR;
--							when novcic => color <= COIN_COLOR;
--							when nista  => color <= BCKG_COLOR;
--                  end case;
--					 elsif (on_collect(2) = '1') then
--						case collect_tip(2) is
--							when bomba  => color <= BOMB_COLOR;
--							when novcic => color <= COIN_COLOR;
--							when nista  => color <= BCKG_COLOR;
--                  end case;
--					 elsif (on_collect(3) = '1') then
--						case collect_tip(3) is
--							when bomba  => color <= BOMB_COLOR;
--							when novcic => color <= COIN_COLOR;
--							when nista  => color <= BCKG_COLOR;
--                  end case;
--					 elsif (on_collect(4) = '1') then
--						case collect_tip(4) is
--							when bomba  => color <= BOMB_COLOR;
--							when novcic => color <= COIN_COLOR;
--							when nista  => color <= BCKG_COLOR;
--                  end case;
--					 elsif (on_collect(5) = '1') then
--						case collect_tip(5) is
--							when bomba  => color <= BOMB_COLOR;
--							when novcic => color <= COIN_COLOR;
--							when nista  => color <= BCKG_COLOR;
--                  end case;
--					 elsif (on_collect(6) = '1') then
--						case collect_tip(6) is
--							when bomba  => color <= BOMB_COLOR;
--							when novcic => color <= COIN_COLOR;
--							when nista  => color <= BCKG_COLOR;
--                  end case;
--					 elsif (on_collect(7) = '1') then
--						case collect_tip(7) is
--							when bomba  => color <= BOMB_COLOR;
--							when novcic => color <= COIN_COLOR;
--							when nista  => color <= BCKG_COLOR;
--                  end case;
--					 else
--                     color <= BCKG_COLOR;
--                end if;
--            end if;
--		end if;
--    end if;
--end process bojenje;

Rout <= color(23 downto 16);
Gout <= color(15 downto  8);
Bout <= color( 7 downto  0);

score_out <= score;
life_out <= life;

-- Instancirane ROM memorije:

bitmapa_surfera : bitmap_surfer port map (std_logic_vector(address), clk, datamem_surfer);

bmp_bombi : bitmap_bomba port map (std_logic_vector(address_bomb), clk, datamem_bomba);

bmp_novcica : bitmap_novcic port map (std_logic_vector(address_novcic), clk, datamem_novcic);

end behavioral;
