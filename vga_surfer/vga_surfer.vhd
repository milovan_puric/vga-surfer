-------------------------------------------------------------------------------------------------------
--
-- Projekat: VGA surfer
-- Autori: Srdjan Djuricic 2016/655, Milovan Puric 2016/207
-- Fakultet  Elektrotehnicki fakultet Univerziteta u Beogradu
-- Katedra: Katedra za elektoniku
--
-------------------------------------------------------------------------------------------------------
--
-- Fajl: vga_surfer.vhd
-- Poslednja verzija napreavljena dana: 6.12.2019.
--
-------------------------------------------------------------------------------------------------------
--
-- Opis: Komponenta u kojoj su instancirane sve komponente koje su potrebne za rad igrice "VGA surfer".
--       Igrica s eprikazuje na VGA displeju. Sa leve strane ekrana iscrtava se surfer dimenzija 48×48 
--       piksela (oblik surfera je posebno definisan kao bitmapa). Surfer može da se nalazi u jednoj od 
--       tri trake – srednjoj, gornjoj ili donjoj. Pozicija surfera se menja pritiskom na tastere gore 
--       i dole. Pozicija surfera duž horizontalne ose je fiksna. Sa drugog kraja ekrana na slučajno 
--       odabranoj poziciji izleću novčići i bombe (takođe definisani kao bitmape). Surfer treba da 
--       pokupi što više novčića (broj skupljenih novčića se ispisuje na  LED displejima) i izbegne 
--       sve bombe. Ako bomba dodirne surfera, surfer gubi jedan od tri života koja ima do kraja igre. 
--       Broj preostalih života se prikazuje u termometarskom kodu na LE diodama. 
--       Brzina kojom izleću novčići i bombe raste kako se povecava skor igrača. 
--
-------------------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity vga_surfer is
port (
	clk_50MHz                               :  in std_logic;
	reset                                   :  in std_logic;
   btn_up, btn_down                        :  in std_logic;
	VGA_CLK                                 : out std_logic;
	VGA_HS, VGA_VS, VGA_BLANK_N, VGA_SYNC_N : out std_logic;
	VGA_R, VGA_G, VGA_B                     : out std_logic_vector( 7 downto 0);
   displays_7seg	                         : out std_logic_vector(20 DOWNTO 0);
   life                                    : out std_logic_vector( 2 downto 0)
);
end vga_surfer;

architecture sturctural of vga_surfer is

component diff is
	port (
		clk   :  in std_logic;
		reset :  in std_logic;
		tast  :  in std_logic;
		one   : out std_logic
	);
end component;

component pll is
	port (
		refclk   : in  std_logic := '0'; 
		rst      : in  std_logic := '0'; 
		outclk_0 : out std_logic         
	);
end component;

component rand_gen is 
port (
  clk           : in  std_logic;
  reset         : in  std_logic;
  enable        : in  std_logic;
  rand          : out integer range 0 to 127
 );
end component;

component pozicija_surfera is
    port(
        clk	    : in std_logic;
        reset   : in std_logic;
        up      : in std_logic;
        down    : in std_logic;
        surfer_top  : out std_logic;
        surfer_mid  : out std_logic;
        surfer_bot  : out std_logic
    );
end component;

component crtanje_i_sudari is
port (
	clk                          :  in std_logic;
	reset                        :  in std_logic;
	hpos                         :  in integer range 0 to 1023;
	vpos                         :  in integer range 0 to 767;
    ref_tick                     :  in std_logic;
	Rout, Gout, Bout             : out std_logic_vector(7 downto 0);
	surf_top, surf_mid, surf_bot :  in std_logic;
	random_num                   :  in integer range 0 to 127;
    score_out                    : out integer range 0 to 999;
    life_out                     : out std_logic_vector(2 downto 0)
);
end component;

component seven_segments is
	generic(
		bits		   :	integer := 10;
		digits		   :	integer :=  3;
		ss_polarity	   :	std_logic := '0'
);
	port(
		clk			   :	in	std_logic;
		reset		   :	in	std_logic;
		number		   :	in	integer;
		displays_7seg  :	out	std_logic_vector(digits*7-1 downto 0)
);
end component;

component vga_sync is
generic(
	H_SYNC    : integer := 136;
	H_BP      : integer := 160;
	H_FP      : integer := 24;
	H_DISPLAY : integer := 1024;
	
	V_SYNC    : integer := 6;
	V_BP      : integer := 29;
	V_FP      : integer := 3;
	V_DISPLAY : integer := 768
);
port(
	clk              :  in std_logic;
	reset            :  in std_logic;
	hsync, vsync     : out std_logic;
	sync_n, blank_n  : out std_logic;
	hpos             : out integer range 0 to H_DISPLAY - 1;
	vpos             : out integer range 0 to V_DISPLAY - 1;
	Rin, Gin, Bin    :  in std_logic_vector(7 downto 0);
	Rout, Gout, Bout : out std_logic_vector(7 downto 0);
	ref_tick         : out std_logic
);
end component;

    signal clk_vga                      : std_logic;
    signal up, down                     : std_logic;
    signal random                       : integer range 0 to 127;
    signal surf_top, surf_mid, surf_bot : std_logic;
    signal hpos                         : integer range 0 to 1023;
    signal vpos                         : integer range 0 to 767;
    signal ref_tick                     : std_logic;
    signal R, G, B                      : std_logic_vector(7 downto 0);
    signal score                        : integer range 0 to 999;
    signal jedinica                     : std_logic;

begin

jedinica <= '1';

VGA_PLL   : pll              port map (clk_50MHz, reset, clk_vga);
D0        : diff             port map (clk_vga, reset, btn_up, up);
D1        : diff             port map (clk_vga, reset, btn_down, down);
SURFER    : pozicija_surfera port map (clk_vga, reset, up, down, surf_top, surf_mid, surf_bot);
GENERATOR : rand_gen         port map (clk_vga, reset, jedinica, random);
CRTANJE   : crtanje_i_sudari port map (clk_vga, reset, hpos, vpos, ref_tick, R, G, B, surf_top, surf_mid, surf_bot, random, score, life);
DISPLEJI  : seven_segments   port map (clk_vga, reset, score, displays_7seg);
SYNC      : vga_sync         port map (clk_vga, reset, VGA_HS, VGA_VS, VGA_SYNC_N, VGA_BLANK_N, hpos, vpos, R, G, B, VGA_R, VGA_G, VGA_B, ref_tick);

VGA_CLK <= clk_vga;

end sturctural;