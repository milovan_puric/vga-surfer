---------------------------------------------------------------------------------------------------
--
-- Projekat: VGA surfer
-- Autori: Srdjan Djuricic 2016/655, Milovan Puric 2016/207
-- Fakultet  Elektrotehnicki fakultet Univerziteta u Beogradu
-- Katedra: Katedra za elektoniku
--
---------------------------------------------------------------------------------------------------
--
-- Fajl: pozicija_surfera.vhd
-- Poslednja verzija napreavljena dana: 2.12.2019.
--
---------------------------------------------------------------------------------------------------
--
-- Opis: Jednostavna masina stanja koja na osnovu dva prekidaca, kao ulaza, odredjuje 
--       poziciju surfera, koja moze biti u tri nivoa (gornjem, srednjem ili donjem).
--
---------------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity pozicija_surfera is
    port(
        clk	    : in std_logic;
        reset   : in std_logic;
        up      : in std_logic;
        down    : in std_logic;
        
        surfer_top  : out std_logic;
        surfer_mid  : out std_logic;
        surfer_bot  : out std_logic
    );
end entity;

architecture behavioral of pozicija_surfera is

    type state_type is (top, mid, bot);
    signal state_reg, next_state : state_type; 
    
begin

    state_transition: process (clk) is
    begin
        if (rising_edge(clk)) then
            if (reset = '1') then
                state_reg <= mid;
            else
                state_reg <= next_state;
            end if;
        end if;
    end process state_transition;

    next_state_logic: process (state_reg, down, up) is
    begin
        case state_reg is
            when top =>
                if (down = '1') then
                    next_state <= mid;
                else 
                    next_state <= top;
                end if;
            when mid =>
                if (down = '1') then
                    next_state <= bot;
                elsif (up = '1') then 
                    next_state <= top;
                else 
                    next_state <= mid;
                end if;
            when bot => 
                if (up = '1') then
                    next_state <= mid;
                else 
                    next_state <= bot;
                end if;
        end case;
        
    end process next_state_logic;

    output_logic: process (state_reg) is
    begin
        case state_reg is
            when top =>
                surfer_top <= '1';
                surfer_mid <= '0';
                surfer_bot <= '0';
            when mid =>
                surfer_top <= '0';
                surfer_mid <= '1';
                surfer_bot <= '0';
            when bot =>
                surfer_top <= '0';
                surfer_mid <= '0';
                surfer_bot <= '1';
        end case;
    
    end process output_logic;
    
end architecture behavioral;